import '../styles/ProductInfo.css';
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

const ProductInfo = () => {
  const [products, setProducts] = useState([]);
  const { id } = useParams();

  const handleSubmit = async () => {
    try {
      const response = await fetch(
        `http://localhost:5000/api/products/find/${id}`,
        {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );

      const resJson = await response.json();
      setProducts(resJson);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => handleSubmit());

  return (
    <>
      <div>
        <div className="mainSection">
          <div className="img">
            <img src={`../assets/${products.img}`} alt={products.brand} />
          </div>
          <div className="productInfo">
            <div className="title">
              <p>
                <b>Name</b>
              </p>
              <p>{products.title}</p>
            </div>
            <div className="brand">
              <p>
                <b>Brand Name</b>
              </p>
              <p>{products.brand}</p>
            </div>
            <div className="price">
              <p>
                <b>Price</b>
              </p>
              <p>{`$${products.price}`}</p>
            </div>
            <div className="size">
              <p>
                <b>Description</b>
              </p>
              <p>{products.size}</p>
            </div>
            <div className="desc">
              <p>{products.desc}</p>
            </div>
            <div className="button" id="tmp">
              <button>Сагсанд хийх</button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProductInfo;
