import '../styles/HeaderContainer.css';
import { Link } from 'react-router-dom';
import setData from '../scripts/setQuery';
const refreshPage = () => {
  window.location.reload(false);
};
const submitHeaderInfo = (_category) => {
  refreshPage();
  let color = 'none';
  let gender = 'none';
  let ageRange = 'none';
  let category = _category;
  let interest = 'none';
  setData(color, gender, ageRange, category, interest);
};
const HeaderContainer = () => {
  return (
    <>
      <div className="headerContainer">
        <div className="left">
          <ul>
            <li>
              <Link to="/Advice" className="button">
                Хайх
              </Link>
              <button
                className="headerButton"
                onClick={() => {
                  submitHeaderInfo('shoes');
                  refreshPage();
                }}
              >
                <Link to="/List" className="button">
                  Гутал
                </Link>
              </button>
              <button
                className="headerButton"
                onClick={() => {
                  submitHeaderInfo('clothes');
                  refreshPage();
                }}
              >
                <Link to="/List" className="button">
                  Хувцас
                </Link>
              </button>
            </li>
          </ul>
        </div>
        <div className="logo">
          <Link to="/" className="button">
            GIFTY
          </Link>
        </div>
        <div className="right">
          <ul>
            <li>
              <button
                className="headerButton"
                onClick={() => {
                  submitHeaderInfo('makeup');
                  refreshPage();
                }}
              >
                <Link to="/List" className="button">
                  Гоо сайхан
                </Link>
              </button>
              <button
                className="headerButton"
                onClick={() => {
                  submitHeaderInfo('jewelry');
                  refreshPage();
                }}
              >
                <Link to="/List" className="button">
                  Гоёл
                </Link>
              </button>
              <button
                className="headerButton"
                onClick={() => {
                  submitHeaderInfo('trend');
                  refreshPage();
                }}
              >
                <Link to="/List" className="button">
                  Тренд
                </Link>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
};
export default HeaderContainer;
