import '../styles/MainContent.css';
import { Link } from 'react-router-dom';
import { useState } from 'react';
import { useEffect } from 'react';
import getScore from '../scripts/scoreCalculator';
import Query from '../schema/query';

const MainContent = () => {
  const [products, setProducts] = useState([]);
  const getProducts = async () => {
    try {
      const response = await fetch(`http://localhost:4000/api/products`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const resJson = await response.json();
      setProducts(resJson);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => getProducts(), []);

  const elements = [];
  let n = products.length;
  // let emptyQuery = new Query('none', 'none', 'none', 'none', 'none');

  for (let i = 0; i < n; i++) {
    products[i].score = Math.random();
    elements.push(products[i]);
  }
  console.log(elements);
  for (let j = 0; j < elements.length; j++)
    for (let i = 1; i < elements.length; i++) {
      if (elements[i - 1].score < elements[i].score) {
        [elements[i], elements[i - 1]] = [elements[i - 1], elements[i]];
      }
    }
  while (elements.length > 24) elements.pop();
  return (
    <>
      <div className="container">
        <div className="recommendText">
          <p>Санал болгож буй бүтээгдэхүүнүүд</p>
        </div>
        <div className="mainContent">
          {elements.map((element) => (
            <div key={element._id} className="button product">
              <Link to={`/Product/${element._id}`} className="container">
                <img
                  className="imgSize"
                  src={`../assets/${element.img}`}
                  alt={element.brand}
                />
                <p className="name">{element.title}</p>
                <p className="price">{`$${element.price}`}</p>
              </Link>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};
export default MainContent;
