import '../styles/List.css';
import { useState } from 'react';
import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import Query from '../schema/query';
import getScore from '../scripts/scoreCalculator';
const List = () => {
  const [products, setProducts] = useState([]);
  const getProducts = async () => {
    try {
      const response = await fetch(`http://localhost:4000/api/products`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const resJson = await response.json();
      setProducts(resJson);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => getProducts(), []);

  let elements = [];
  let n = products.length;

  let color = sessionStorage.getItem('color');
  let randomize = sessionStorage.getItem('randomize');
  let gender = sessionStorage.getItem('gender');
  let ageRange = sessionStorage.getItem('ageRange');
  let category = sessionStorage.getItem('category');
  let interest = sessionStorage.getItem('interest');
  let curQuery = new Query(color, gender, ageRange, category, interest);
  for (let i = 0; i < n; i++) {
    products[i].score = getScore(products[i], curQuery);
    if (randomize) {
      products[i].score = Math.random();
    }
    if (
      curQuery.category === 'none' ||
      products[i].categories.includes(curQuery.category)
    )
      elements.push(products[i]);
  }
  for (let j = 0; j < elements.length; j++)
    for (let i = 1; i < elements.length; i++) {
      if (elements[i - 1].score < elements[i].score) {
        [elements[i], elements[i - 1]] = [elements[i - 1], elements[i]];
      }
    }
  while (elements.length > 24) elements.pop();

  return (
    <>
      <div className="container">
        <div className="listContent">
          {elements.map((element) => (
            <div key={element._id} className="button product">
              <Link to={`/Product/${element._id}`} className="container">
                <img
                  className="imgSize"
                  src={`../assets/${element.img}`}
                  alt={element.brand}
                />
                <p className="name">{element.title}</p>
                <p className="price">{`$${element.price}`}</p>
              </Link>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};
export default List;
