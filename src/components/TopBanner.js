import '../styles/TopBanner.css';
const TopBanner = () => {
  return (
    <>
      <div className="topBanner button">
        <div className="name">
          <p>NIKE FREE</p>
        </div>
        <div className="info">
          <h1>
            Өнгөрсөн долоо хоногийн
            <br />
            онцлох бүтээгдэхүүн
          </h1>
        </div>
        <img src="../assets/topBanner.jpg" alt="topBanner" />
      </div>
    </>
  );
};
export default TopBanner;
