import '../styles/AddProduct.css';

const AddProduct = () => {
  const addImg = async () => {
    try {
      const response = await fetch(`http://localhost:5000/api/img`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <div class="main-container">
        <div class="img">
          <form action="/action_page.php">
            <label for="img">img</label>
            <input type="file" id="img" name="img" accept="image/*" />
          </form>
        </div>
        <div class="text">
          <b>name</b>
          <input type="text" />
        </div>
        <button onClick={addImg()}>Nemeh oonh</button>
      </div>
    </>
  );
};
export default AddProduct;
