import '../styles/Footer.css';
import { Link } from 'react-router-dom';
import setData from '../scripts/setQuery';
const refreshPage = () => {
  window.location.reload(false);
};
const submitHeaderInfo = (_category) => {
  refreshPage();
  let color = 'none';
  let gender = 'none';
  let ageRange = 'none';
  let category = _category;
  let interest = 'none';
  setData(color, gender, ageRange, category, interest);
};
const Footer = () => {
  return (
    <>
      <div className="footer">
        <div className="logo">
          <h2>GIFTY</h2>
        </div>
        <div className="contact">
          <h3>Холбоо барих</h3>
          <p>Facebook</p>
          <p>Email</p>
          <p>Phone</p>
        </div>
        <div className="client">
          <h3>Хамтрагч байгууллагууд</h3>
          <p> Zara</p>
          <p>Nike</p>
          <p>Converse</p>
          <p>Victoria’s secret</p>
          <p>Jared</p>
        </div>
        <div className="menu">
          <h3>Хөтөч</h3>
          {/* 
           
           
              <button
                className="footer"
                onClick={() => {
                  submitHeaderInfo('shoes');
                  refreshPage();
                }}
              >
                <Link to="/List" className="button">
                  Гутал
                </Link>
              </button>
           
           */}
          <button
            className="footerButton"
            onClick={() => {
              submitHeaderInfo('shoes');
              refreshPage();
            }}
          >
            <Link to="/List" className="button">
              Гутал
            </Link>
          </button>
          <button
            className="footerButton"
            onClick={() => {
              submitHeaderInfo('jewelry');
              refreshPage();
            }}
          >
            <Link to="/List" className="button">
              Гоёл
            </Link>
          </button>
          <button
            className="footerButton"
            onClick={() => {
              submitHeaderInfo('clothes');
              refreshPage();
            }}
          >
            <Link to="/List" className="button">
              Хувцас
            </Link>
          </button>
          <button
            className="footerButton"
            onClick={() => {
              submitHeaderInfo('trend');
              refreshPage();
            }}
          >
            <Link to="/List" className="button">
              Тренд
            </Link>
          </button>
          <button
            className="footerButton"
            onClick={() => {
              submitHeaderInfo('makeup');
              refreshPage();
            }}
          >
            <Link to="/List" className="button">
              Гоо сайхан
            </Link>
          </button>
        </div>
      </div>
    </>
  );
};
export default Footer;
