import '../styles/InputForm.css';
import setData from '../scripts/setQuery';
import { Link } from 'react-router-dom';
const submitInputForm = () => {
  let color = document.getElementById('color').value;
  let gender = document.getElementById('gender').value;
  let ageRange = document.getElementById('age').value;
  let category = document.getElementById('category').value;
  let interest = document.getElementById('interest').value;
  setData(color, gender, ageRange, category, interest);
};
const InputForm = () => {
  return (
    <>
      <div className="adviceContent">
        <div className="adviceContentContainer">
          <form>
            <p>Хүйс</p>
            <select className="gender" id="gender" name="gender">
              <option value="none"></option>
              <option value="male">Эрэгтэй</option>
              <option value="female">Эмэгтэй</option>
            </select>
          </form>
          <form>
            <p>Нас</p>
            <select className="age" id="age" name="age">
              <option value="none"></option>
              <option value="baby">1-4</option>
              <option value="kid">5-12</option>
              <option value="junior">13-18</option>
              <option value="adult">19-25</option>
              <option value="adult">26-50</option>
              <option value="adult">50+</option>
            </select>
          </form>
          <form>
            <p>Сонирхол</p>
            <select className="interest" id="interest" name="interest">
              <option value="none"></option>
              <option value="art">Урлаг</option>
              <option value="sport">Спорт</option>
              <option value="anime">Аниме</option>
            </select>
          </form>
          <form>
            <p>Өнгө</p>
            <select className="color" id="color" name="color">
              <option value="none"></option>
              <option value="red">Улаан</option>
              <option value="blue">Цэнхэр</option>
              <option value="black">Хар</option>
            </select>
          </form>
          <form>
            <p>Төрөл</p>
            <select className="category" id="category" name="category">
              <option value="none"></option>
              <option value="trend">Тренд</option>
              <option value="shoes">Гутал</option>
              <option value="game">Тоглоом</option>
              <option value="jewelry">Гоёл</option>
              <option value="clothes">Хувцас</option>
              <option value="food">Хоол амттан</option>
              <option value="makeup">Гоо сайхан</option>
            </select>
          </form>
          <button onClick={submitInputForm} className="inputFormButton">
            <Link to="/List" className="button">
              Үргэлжлүүлэх
            </Link>
          </button>
        </div>
      </div>
    </>
  );
};
export default InputForm;
