import '../styles/RecommendSec.css';
import setData from '../scripts/setQuery';
const RecommendSec = () => {
  return (
    <>
      <div className="content-title">
        <h2>Бэлэгний санаанууд</h2>
      </div>
      <div className="topSection">
        <div className="centerContent">
          <div className="top-section">
            <p className="topSectionTxt">Төрсөн өдөр</p>
            <button
              onClick={() => {
                setData('none', 'none', 'none', 'birthday', 'none');
                window.location.href = 'List';
                console.log('HELLO');
              }}
              className="topSectionButton"
            >
              Үзэх
            </button>
            <img
              className="topSectionImg"
              src="../assets/jewelry1.jpg"
              alt="jewelry1"
            />
          </div>
          <div className="top-section">
            <p className="topSectionTxt">Ойн баярын өдөр</p>
            <button
              onClick={() => {
                setData('none', 'none', 'none', 'anniversary', 'none');
                window.location.href = 'localhost:3000/List';
                console.log('HELLO');
              }}
              className="topSectionButton"
            >
              Үзэх
            </button>
            <img
              className="topSectionImg"
              src="../assets/gift.jpg"
              alt="gift"
            />
          </div>
          <div className="top-section">
            <p className="topSectionTxt">Тэмдэглэлт өдөр</p>
            <button
              onClick={() => {
                setData('none', 'none', 'none', 'festival', 'none');
                window.location.href = 'localhost:3000/List';
                console.log('HELLO');
              }}
              className="topSectionButton"
            >
              Үзэх
            </button>
            <img
              className="topSectionImg"
              src="../assets/annives.jpg"
              alt="annives"
            />
          </div>
        </div>
      </div>
    </>
  );
};
export default RecommendSec;
