/*
    in order for this to work 
    product has to have {
        gender,
        interest,
        color,
        ageRange,
        category,
        score (?????) main important thing
    }
*/
const getScore = (product, query) => {
  let multiplier = 0; // total must be 100
  if (query.color === 'none' || product.color === query.color) {
    multiplier += 10;
  }
  if (query.gender === 'none' || product.gender === query.gender) {
    multiplier += 40;
  }
  if (query.ageRange === 'none' || product.ageRange === query.ageRange) {
    multiplier += 20;
  }
  if (
    query.category === 'none' ||
    product.categories.includes(query.category)
  ) {
    multiplier += 15;
  }
  if (query.interest === 'none' || product.interest === query.interest) {
    multiplier += 15;
  }
  return multiplier * (product.score === undefined ? 0 : product.score + 100);
};
export default getScore;
