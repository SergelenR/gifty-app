function setData(color, gender, ageRange, category, interest) {
  // console.log('setData: ', color, gender, ageRange, category, interest);
  sessionStorage.randomize = false;
  if (category === 'trend') {
    sessionStorage.randomize = true;
    category = 'none';
  }
  sessionStorage.setItem('color', color);
  sessionStorage.setItem('gender', gender);
  sessionStorage.setItem('ageRange', ageRange);
  sessionStorage.setItem('category', category);
  sessionStorage.setItem('interest', interest);
}
export default setData;
