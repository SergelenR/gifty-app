import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import HomePage from "./Pages/Home";
import AdvicePage from "./Pages/Advice";
import ProductPage from "./Pages/Product";
import ProductList from "./Pages/ProductList";
import AddProductPage from "./Pages/AddProductAd";
ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Routes>
        <Route path="/" element={<HomePage />}></Route>
        <Route path="/Advice" element={<AdvicePage />}></Route>
        <Route path="/Product/:id" element={<ProductPage />}></Route>
        <Route path="/List" element={<ProductList />}></Route>
        <Route path="/AddProduct" element={<AddProductPage />}></Route>
      </Routes>
    </Router>
  </React.StrictMode>,
  document.getElementById("root")
);
