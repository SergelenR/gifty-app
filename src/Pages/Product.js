import Footer from "../components/Footer";
import HeaderContainer from "../components/HeaderContainer";
import ProductInfo from "../components/ProductInfo";

const ProductPage = () => {
  return (
    <>
      <HeaderContainer />
      <ProductInfo />
      <Footer />
    </>
  );
};
export default ProductPage;
