import Footer from "../components/Footer";
import HeaderContainer from "../components/HeaderContainer";
import List from "../components/List";

const ProductList = () => {
  return (
    <>
      <HeaderContainer />
      <List />
      <Footer />
    </>
  );
};
export default ProductList;
