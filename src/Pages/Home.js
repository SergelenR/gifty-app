import Footer from "../components/Footer";
import HeaderContainer from "../components/HeaderContainer";
import MainContent from "../components/MainContent";
import RecommendSec from "../components/RecommendSec";
import TopBanner from "../components/TopBanner";
import "../styles/Global.css";
const HomePage = () => {
  return (
    <div className="HomePage">
      <HeaderContainer />
      <TopBanner />
      <RecommendSec />
      <MainContent />
      <Footer />
    </div>
  );
};

export default HomePage;
