import Footer from "../components/Footer";
import HeaderContainer from "../components/HeaderContainer";
import InputForm from "../components/InputForm";

const AdvicePage = () => {
  return (
    <>
      <HeaderContainer />
      <InputForm />
      <Footer />
    </>
  );
};
export default AdvicePage;
