function Query(color, gender, ageRange, category, interest) {
  this.color = color;
  this.gender = gender;
  this.ageRange = ageRange;
  this.category = category;
  this.interest = interest;
}
export default Query;
